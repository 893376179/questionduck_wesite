from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "questionduck_wesite.users"
    verbose_name = _("Users")

    def ready(self):
        try:
            import questionduck_wesite.users.signals  # noqa F401
        except ImportError:
            pass
